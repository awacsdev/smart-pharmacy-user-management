FROM openjdk:13-jdk-alpine
ARG JAR_FILE=target/*.jar
WORKDIR /
COPY . /
COPY ${JAR_FILE} app.jar
ADD Dockerfile.key /keys.txt
RUN base64 -d /keys.txt > /keys.json
ENV GOOGLE_APPLICATION_CREDENTIALS="/keys.json"
EXPOSE 9393
ENTRYPOINT ["java","-Dspring.profiles.active=prod", "-Dawacs_grpc_host=authserver", "-jar","/app.jar"]
