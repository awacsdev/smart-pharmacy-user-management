package com.aiocdawacs;

import java.security.Principal;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AppController {
	@GetMapping("mello")
	public ModelAndView welcome(Principal principal) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("welcome");
		mav.addObject("name", principal.getName());
		return mav;
	}
	
	@GetMapping("hello")
	public ModelAndView dashboardService(@RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient authorizedClient,
			@AuthenticationPrincipal OAuth2User oauth2User) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("welcome");
		
		mav.addObject("userName", oauth2User.getName());
		mav.addObject("clientName", oauth2User.getAttributes().get("name"));
		mav.addObject("clientName2", authorizedClient.getClientRegistration().getClientName());
		mav.addObject("userAttributes", oauth2User.getAttributes());
		
		return mav;
	}
	
}
